# Data Management

NERSC provides its users with the means to store, manage, and share their research data products.

In addition to systems specifically tailored for data-intensive computations, we provide a variety of storage resources optimized for different phases of the data lifecycle; tools to enable users to manage, protect, and control their data; high-speed networks for intra-site and inter-site (ESnet) data transfer; gateways and portals for publishing data for broad consumption; and consulting services to help users craft efficient data management processes for their projects.

## OSTP/Office of Science Data Management Requirements

Project Principal Investigators are responsible for meeting OSTP (Office of Science and Technology Policy) and DOE Office of Science data management requirements for long-term data sharing and preservation. The OSTP has issued a memorandum on Increasing Access to the Results of Federally Funded Scientific Research (http://www.whitehouse.gov/sites/default/files/microsites/ostp/ostp_public_access_memo_2013.pdf) and the DOE has issued a Statement on Digital Data Management.

NERSC resources are intended for users with active allocations, and as described below, NERSC cannot guarantee long-term data access without a prior, written, service-level agreement.  Please carefully consider these policies, including their limitations and restrictions, as you develop your data management plan.


