# Data visualization

Scientific Visualization is the process of creating visual imagery from raw scientific data. NERSC supports the VisIt and Paraview tools for visualizing and interacting with generic scientific datasets. NCAR Graphics Library is provided (as a specialized package) for climate data. 

The field of information visualization deals with rendering datasets that do not necessarily map onto a natural 2D or 3D co-ordinate system. R (ggplot2) and python (matplotlib) provide capabilities for information visualization. 

* Paraview
* Visit
* NCAR Graphics
