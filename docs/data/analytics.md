# Analytics

Analytics is key to gaining insights from massive, complex datasets.
NERSC provides general purpose analytics (iPython, Spark, MATLAB, IDL, 
Mathematica, ROOT), statistics (R), machine learning, and imaging tools.
