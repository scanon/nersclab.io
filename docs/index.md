# NERSC Technical Documentation

## Quick links/ FAQ

 1. [Obtaining an Account](accounts/index.md#obtaining-an-account)
 1. [Building Software](development/compilers.md)
 1. [Running Jobs](jobs/index.md)
 1. [Managing Data](data/management.md)
 1. [Reset my password](accounts/passwords/index.html#forgotten-passwords)

!!!warning "BETA"
	Migration of content from [http://www.nersc.gov/users/](http://www.nersc.gov/users/) is underway.
