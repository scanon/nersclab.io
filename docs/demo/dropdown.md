# How to run a job

The main examples are for Cori KNL. Scripts for Edison and Cori Haswell are 
available from the dropdowns.

## Running an OpenMP job


```shell
--8<-- "docs/demo/example/run-cori-knl.sh"
```

??? example "Edison"
    ```shell
    --8<-- "docs/demo/example/run-edison.sh"
    ```

??? example "Cori Haswell"
    ```shell
    --8<-- "docs/demo/example/run-cori.sh"
    ```

	
