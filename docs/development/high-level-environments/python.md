# Python

Python is an interpreted, general-purpose high-level programming language. Python is available to Cori and Edison users through the Anaconda distribution. 
Using Python at NERSC requires at least one "module load" command. 
Using the system-provided Python (from /usr/bin) is strongly discouraged except for the simplest tasks, as it can be a much older version of Python than that provided by NERSC for users.

Python users may also be interested in the experimental IPython/Jupyter notebook web application service.

