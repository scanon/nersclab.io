Hierarchical Data Format version 5 (HDF5) is a set of file formats,
libraries, and tools for storing and managing large scientific
datasets. Originally developed at the National Center for
Supercomputing Applications, it is currently supported by the
non-profit HDF Group.

HDF5 is different product from previous versions of software named
HDF, representing a complete redesign of the format and library.  It
also includes improved support for parallel I/O. The HDF5 file format
is not compatible with HDF 4.x versions. You can use the 'h5toh4' and
'h4toh5' converters that are available on all NERSC machines.
