# NX

NX is a computer program that handles remote X Window System connections and it provides three benefits for NERSC users:

## Benefits

### Speed
 NX can greatly improve the performance of X Windows, allowing users with slow, high latency connections (e.g. on cell phone network, traveling in Africa) to use complex X Windows programs (such as rotating a plot in Matlab). 

### Session

NX provides sessions that allow a user to disconnect from the session and reconnect to it at a later time while keeping the state of all running applications inside the session.
 
### Desktop

NX gives users a virtual desktop that's running at NERSC. You can customize the desktop according to your work requirement 

## Connecting

You can connect to NX via the NX Client or by pointing your browser at [nxcloud01](https://nxcloud01.nersc.gov). The browser interface is still experimental and can sometimes be slower than connecting via the client. Please let us know by opening a ticket if you encounter any issues with it.
